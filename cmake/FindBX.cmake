
SET(BX_SEARCH_PATHS
	~/Library/Frameworks
	/Library/Frameworks
	/usr/local
	/usr
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
)

FIND_PATH(BX_INCLUDE_DIR bx.h
	HINTS
	$ENV{BX_DIR}
	PATH_SUFFIXES include/bx include
	PATHS ${BX_SEARCH_PATHS}
)

FIND_LIBRARY(BX_LIBRARY_TEMP
	NAMES bx
	HINTS
	$ENV{BX_DIR}
	PATH_SUFFIXES lib64 lib
	PATHS ${BX_SEARCH_PATHS}
)

IF(BX_LIBRARY_TEMP)
	# Set the final string here so the GUI reflects the final state.
	SET(BX_LIBRARY ${BX_LIBRARY_TEMP} CACHE STRING "Where the BX Library can be found")
	# Set the temp variable to INTERNAL so it is not seen in the CMake GUI
	SET(BX_LIBRARY_TEMP "${BX_LIBRARY_TEMP}" CACHE INTERNAL "")
ENDIF(BX_LIBRARY_TEMP)

INCLUDE(FindPackageHandleStandardArgs)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(bx REQUIRED_VARS BX_LIBRARY BX_INCLUDE_DIR)
