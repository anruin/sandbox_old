
SET(BGFX_SEARCH_PATHS
	~/Library/Frameworks
	/Library/Frameworks
	/usr/local
	/usr
	/sw # Fink
	/opt/local # DarwinPorts
	/opt/csw # Blastwave
	/opt
)

FIND_PATH(BGFX_INCLUDE_DIR bgfx.h
	HINTS
	$ENV{BGFX_DIR}
	PATH_SUFFIXES include/bgfx include
	PATHS ${BGFX_SEARCH_PATHS}
)

FIND_LIBRARY(BGFX_LIBRARY_TEMP
	NAMES bgfx
	HINTS
	$ENV{BGFX_DIR}
	PATH_SUFFIXES lib64 lib
	PATHS ${BGFX_SEARCH_PATHS}
)

IF(BGFX_LIBRARY_TEMP)
	# Set the final string here so the GUI reflects the final state.
	SET(BGFX_LIBRARY ${BGFX_LIBRARY_TEMP} CACHE STRING "Where the BGFX Library can be found")
	# Set the temp variable to INTERNAL so it is not seen in the CMake GUI
	SET(BGFX_LIBRARY_TEMP "${BGFX_LIBRARY_TEMP}" CACHE INTERNAL "")
ENDIF(BGFX_LIBRARY_TEMP)

INCLUDE(FindPackageHandleStandardArgs)

FIND_PACKAGE_HANDLE_STANDARD_ARGS(bgfx REQUIRED_VARS BGFX_LIBRARY BGFX_INCLUDE_DIR)
