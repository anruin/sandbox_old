//
// Copyright (c) E. A. Pristavka, 2017
//

#include <stdlib.h>
#include <string.h>
#include "common/vector.h"
#include <stdio.h>
#include <assert.h>
#include <memory.h>

#define PTR_TO_VALUE(type, value) *(type *)value

static void TDestructor(void *p) {
    if (p)
        free(p);
}

static void CVector_PrintInt(CVector *v) {
    void *p;
    for (size_t i = 0; i < v->nCount; i++) {
        CVector_Get(v, i, &p);
        printf("%d\t", *(int *) p);
    }
    printf("\n");
}

static void CVector_PrintStr(CVector *v) {
    void *p;
    for (size_t i = 0; i < CVector_Count(v); i++) {
        CVector_Get(v, i, &p);
        printf("%s\t", (char *) p);
    }
    printf("\n");
}


typedef struct {
    char *name;
    int value;
    // int padding;
} TPair;

static void CVector_PrintStruct(CVector *v) {
    void *p;
    for (size_t i = 0; i < CVector_Count(v); i++) {
        CVector_Get(v, i, &p);
        fprintf(stderr, "%s:%d ", ((TPair *) p)->name, ((TPair *) p)->value);
     //   printf("%d ", ((TPair *) p)->value);
    }
    fprintf(stderr, "\n");
}

static void TVector_Integers() {
    size_t size = 10;
    size_t i;
    int nElement;
    void *pElement = NULL;

    CVector *v = CVector_New(10, TDestructor);

    assert(CVector_Empty(v));

    for (i = 0; i <= size; i++) {
        CVector_PushBack(v, &i, sizeof(size_t));
    }

    assert(!CVector_Empty(v));
    assert(size + 1 == CVector_Count(v));

    for (i = 0; i <= size; i++) {
        CVector_Get(v, i, &pElement);
        nElement = *(int *) pElement;
        assert(nElement == i);
        free(pElement);
    }

    CVector_PrintInt(v);

    CVector_Remove(v, 0);
    assert(size == CVector_Count(v));
    CVector_Get(v, 0, &pElement);
    nElement = *(int *) pElement;
    assert(nElement == 1);
    free(pElement);

    CVector_PrintInt(v);

    size = CVector_Count(v);
    CVector_Remove(v, size / 2);
    assert(size - 1 == CVector_Count(v));
    CVector_Get(v, size / 2, &pElement);
    nElement = *(int *) pElement;
    assert(nElement == size / 2 + 2);
    free(pElement);

    CVector_PrintInt(v);

    size = CVector_Count(v);
    CVector_Remove(v, size - 1);
    assert(size - 1 == CVector_Count(v));
    size = CVector_Count(v);
    CVector_Get(v, size - 1, &pElement);
    nElement = *(int *) pElement;
    assert(nElement == 9);
    free(pElement);

    CVector_PrintInt(v);

    i = 900;
    CVector_Insert(v, 5, &i, sizeof(int));
    CVector_Get(v, 5, &pElement);
    nElement = PTR_TO_VALUE(int, pElement);
    assert(nElement == i);
    free(pElement);

    CVector_PrintInt(v);

    CVector_Get(v, 6, &pElement);
    nElement = PTR_TO_VALUE(int, pElement);
    assert(nElement == 7);
    free(pElement);

    CVector_Delete(v);
}

static void TVector_Strings() {
    size_t size = 10;
    wchar_t *input[11];
    size_t i = 0;
    wchar_t *pElement, nElement;
    void *pVoid = NULL;

    CVector *vector = CVector_New(8, TDestructor);
    assert(CVector_Empty(vector));

    input[0] = L"х";
    input[1] = L"о";
    input[2] = L"л";
    input[3] = L"о";
    input[4] = L"д";
    input[5] = L"и";
    input[6] = L"л";
    input[7] = L"ь";
    input[8] = L"н";
    input[9] = L"и";
    input[10] = L"к";

    for (i = 0; i < size; i++) {
        CVector_PushBackString(vector, input[i]);
    }
    assert(!CVector_Empty(vector));
    assert(CVector_Count(vector) == size);

    for (i = 0; i < size; i++) {
        CVector_Get(vector, i, &pVoid);
        pElement = (char *) pVoid;
        assert(strcmp(pElement, input[i]) == 0);
    }

    CVector_PrintStr(vector);

    CVector_Delete(vector);
}

static void TVector_Structs() {
    size_t size = 3;
    size_t i = 0;
    TPair *pElement;
    void *pVoid = NULL;

    CVector *vector = CVector_New(8, TDestructor);
    assert(CVector_Empty(vector));

    for (i = 0; i < size; i++) {
        TPair *pair = malloc(sizeof(TPair));
        memcpy(&pair->value, &i, sizeof(size_t));
        char buffer[8];
        sprintf(buffer, "%d", (int)i);
        //pair->value = 0xcececece;
        pair->name = strdup(buffer);
       // pair->padding = 0xffffffff;

        fprintf(stderr, "%s %d \t", pair->name, pair->value);

        CVector_PushBack(vector, pair, sizeof(TPair));
    }

    CVector_PrintStruct(vector);
}

int main(int argc, char *argv[]) {

    printf("Integers test\n");
    TVector_Integers();

    printf("Strings test\n");
    TVector_Strings();

    printf("Structs test\n");
    TVector_Structs();

    printf("All tests passed\n\n");

}