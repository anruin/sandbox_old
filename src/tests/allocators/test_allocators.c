//
// Copyright (c) E. A. Pristavka, 2017
//

#include <stdio.h>
#include <common/allocators/pool.h>
#include <string.h>

#define TEST(str) fprintf(stderr, "%s\n", #str);
#define END_TEST() fprintf(stderr, "\n");

int main(int argc, char *argv[]) {
    const u32 nAlignment = 8;
    const u32 nSize = 8;
    const u32 nCount = 2;

    TEST("Allocate");
    APoolAllocator poolAllocator = { 0 };
    APoolAllocator_OnCreate(&poolAllocator, nCount, nSize);

    char *p = APoolAllocator_Allocate(&poolAllocator);
    for (i32 i = 0; i < nCount * nSize; i++)
        p[i] = 'a';
    p[nCount * nSize] = '\0';
    fprintf(stderr, "%s %zu\n", p, strlen(p));
    END_TEST()

    TEST("Deallocate")
    APoolAllocator_Free(&poolAllocator, p);
    fprintf(stderr, "%s %zu\n", p, strlen(p));
    END_TEST()

    TEST("Destroy")
    APoolAllocator_Destroy(&poolAllocator);
    fprintf(stderr, "%s %zu\n", p, strlen(p));
    END_TEST()
}


