//
// Copyright (c) E. A. Pristavka, 2017
//

#include <geometry/vector3.h>
#include <geometry/matrix4.h>
#include <stdio.h>
#include <geometry/base.h>
#include <geometry/transform.h>

#define TEST_VECTOR
#define TEST(str) fprintf(stderr, "%s\n", #str);

void TestVector3();
void TestMatrix4();
void TestTransform();

int main(int argc, char* argv[]) {
    TestTransform();
}

void TestTransform() {
    CVector3 *pPosition = CVector3_New(NULL);
    CVector3_Fill(pPosition, 1, 1, 1);

    CVector3 *pRotation = CVector3_New(0);
    CVector3_Fill(pRotation, (CReal)M_PI, (CReal)M_PI_4, (CReal)0);

    CVector3 *pScale = CVector3_New(0);
    CVector3_Fill(pScale, 1, 1, 1);

    TEST("New transform");
    CTransform *pTransform = CTransform_New(NULL);
    CTransform_Print(pTransform);
    fprintf(stderr, "\n");

    TEST("Transform set position");
    pTransform->pPosition.x = 1.012345f;
    pTransform->pPosition.y = 2.123456f;
    pTransform->pPosition.z = 3.456789f;
    CTransform_Print(pTransform);
    fprintf(stderr, "\n");

    TEST("Transform set position, rotation, scale");
    CTransform_SetScale(pTransform, pScale);
    CTransform_SetRotation(pTransform, pRotation);
    CTransform_SetPosition(pTransform, pPosition);

    CTransform_Print(pTransform);
    fprintf(stderr, "\n");
}

void TestVector3() {

    TEST("New vector");
    CVector3 *pVector = CVector3_New(NULL);
    CVector3_Print(pVector);
    fprintf(stderr, "\n");

    TEST("Fill vector");
    CVector3_Fill(pVector, 1, 1, 1);
    CVector3_Print(pVector);
    fprintf(stderr, "\n");

    TEST("Vector length");
    CReal len = CVector3_Length(pVector);
    CReal_Print(len);
    fprintf(stderr, "\n");

}

void TestMatrix4() {

    CMatrix4 *pMatrix = CMatrix4_New(NULL);
    CMatrix4_Print(pMatrix);
    fprintf(stderr, "\n");

    CMatrix4_Identity(pMatrix);
    CMatrix4_Print(pMatrix);
    fprintf(stderr, "\n");

    //CMatrix4_Scale(pMatrix, 2, 2, 2);
    //CMatrix4_Print(pMatrix);
    //NEWLINE();

    CMatrix4 *pMatrix2 = CMatrix4_New(0);
    CMatrix4_Identity(pMatrix2);
    pMatrix2->matrix[0] = 2;
    pMatrix2->matrix[5] = 2;
    pMatrix2->matrix[10] = 2;
    pMatrix2->matrix[12] = 3;

    CMatrix4_Multiply(pMatrix, pMatrix, pMatrix2);
    CMatrix4_Print(pMatrix);
    fprintf(stderr, "\n");

}
