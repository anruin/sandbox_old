//
// Copyright (c) E. A. Pristavka, 2017
//

#include <stdlib.h>
#include <memory.h>
#include "vector3.h"
#include "matrix4.h"

CMatrix4 *CMatrix4_New(CMatrix4 *pSource) {
    CMatrix4 *pMatrix = calloc(1, sizeof(CMatrix4));

    if (pSource)
        memcpy(pMatrix, pSource, sizeof(CMatrix4));

    return pMatrix;
}

CMatrix4 *CMatrix4_Delete(CMatrix4 *pSource) {
    free(pSource);
    return NULL;
}

CMatrix4 *CMatrix4_Set(CMatrix4 *pDest, const CMatrix4 *pSource) {
    if (pSource)
        memcpy(pDest, pSource, sizeof(CMatrix4));

    return pDest;
}

CMatrix4 *CMatrix4_Identity(CMatrix4 *pDest) {
    memset(pDest->matrix, 0, sizeof(CReal) * 16);
    pDest->matrix[0] = pDest->matrix[5] = pDest->matrix[10] = pDest->matrix[15] = 1;
    return pDest;
}

CMatrix4 *CMatrix4_Inverse(CMatrix4 *pDest, const CMatrix4 *pSource) {
    return NULL;
}

CMatrix4 *CMatrix4_Transpose(CMatrix4 *pDest, const CMatrix4 *pSource);

CMatrix4 *CMatrix4_Multiply(CMatrix4 *pDest, const CMatrix4 *pFirst, const CMatrix4 *pSecond) {
    CReal mat[16];

    const CReal *m1 = pFirst->matrix, *m2 = pSecond->matrix;

    mat[0] = m1[0] * m2[0] + m1[4] * m2[1] + m1[8] * m2[2] + m1[12] * m2[3];
    mat[1] = m1[1] * m2[0] + m1[5] * m2[1] + m1[9] * m2[2] + m1[13] * m2[3];
    mat[2] = m1[2] * m2[0] + m1[6] * m2[1] + m1[10] * m2[2] + m1[14] * m2[3];
    mat[3] = m1[3] * m2[0] + m1[7] * m2[1] + m1[11] * m2[2] + m1[15] * m2[3];

    mat[4] = m1[0] * m2[4] + m1[4] * m2[5] + m1[8] * m2[6] + m1[12] * m2[7];
    mat[5] = m1[1] * m2[4] + m1[5] * m2[5] + m1[9] * m2[6] + m1[13] * m2[7];
    mat[6] = m1[2] * m2[4] + m1[6] * m2[5] + m1[10] * m2[6] + m1[14] * m2[7];
    mat[7] = m1[3] * m2[4] + m1[7] * m2[5] + m1[11] * m2[6] + m1[15] * m2[7];

    mat[8] = m1[0] * m2[8] + m1[4] * m2[9] + m1[8] * m2[10] + m1[12] * m2[11];
    mat[9] = m1[1] * m2[8] + m1[5] * m2[9] + m1[9] * m2[10] + m1[13] * m2[11];
    mat[10] = m1[2] * m2[8] + m1[6] * m2[9] + m1[10] * m2[10] + m1[14] * m2[11];
    mat[11] = m1[3] * m2[8] + m1[7] * m2[9] + m1[11] * m2[10] + m1[15] * m2[11];

    mat[12] = m1[0] * m2[12] + m1[4] * m2[13] + m1[8] * m2[14] + m1[12] * m2[15];
    mat[13] = m1[1] * m2[12] + m1[5] * m2[13] + m1[9] * m2[14] + m1[13] * m2[15];
    mat[14] = m1[2] * m2[12] + m1[6] * m2[13] + m1[10] * m2[14] + m1[14] * m2[15];
    mat[15] = m1[3] * m2[12] + m1[7] * m2[13] + m1[11] * m2[14] + m1[15] * m2[15];


    memcpy(pDest->matrix, mat, sizeof(CReal)*16);

    return pDest;
}

CMatrix4 *CMatrix4_Translation(CMatrix4 *pDest, const CVector3 *pSource) {
    memset(pDest->matrix, 0, sizeof(CReal) * 16);

    pDest->matrix[0] = 1.0f;
    pDest->matrix[5] = 1.0f;
    pDest->matrix[10] = 1.0f;
    pDest->matrix[15] = 1.0f;

    pDest->matrix[12] = pSource->x;
    pDest->matrix[13] = pSource->y;
    pDest->matrix[14] = pSource->z;

    return pDest;
}

CMatrix4 *CMatrix4_RotationX(CMatrix4 *pDest, const CReal fRadians) {
    pDest = CMatrix4_Identity(pDest);

    pDest->matrix[5] = cos_r(fRadians);
    pDest->matrix[6] = sin_r(fRadians);
    pDest->matrix[9] = -sin_r(fRadians);
    pDest->matrix[10] = cos_r(fRadians);

    return pDest;
}

CMatrix4 *CMatrix4_RotationY(CMatrix4 *pDest, const CReal fRadians) {
    pDest = CMatrix4_Identity(pDest);

    pDest->matrix[0] = cos_r(fRadians);
    pDest->matrix[2] = -sin_r(fRadians);
    pDest->matrix[8] = sin_r(fRadians);
    pDest->matrix[10] = cos_r(fRadians);

    return pDest;
}

CMatrix4 *CMatrix4_RotationZ(CMatrix4 *pDest, const CReal fRadians) {
    pDest = CMatrix4_Identity(pDest);

    pDest->matrix[0] = cos_r(fRadians);
    pDest->matrix[1] = sin_r(fRadians);
    pDest->matrix[4] = -sin_r(fRadians);
    pDest->matrix[5] = cos_r(fRadians);

    return pDest;
}

CMatrix4 *CMatrix4_Scale(CMatrix4 *pDest, const CVector3 *pSource) {

    pDest = CMatrix4_Identity(pDest);

    pDest->matrix[0] = pSource->x;
    pDest->matrix[5] = pSource->y;
    pDest->matrix[10] = pSource->z;

    return pDest;
}

CMatrix4 *CMatrix4_Perspective(CMatrix4 *pDest, CReal fFOV, CReal fAspect, CReal fNearClip, CReal fFarClip);
CMatrix4 *CMatrix4_Orthographic(CMatrix4 *pDest, CReal fLeft, CReal fRight, CReal fBottom, CReal fTop, CReal fNearClip, CReal fFarClip);
CMatrix4 *CMatrix4_LookAt(CMatrix4 *pDest, const struct CVector3 *pCamera, const struct CVector3 *pTarget, const struct CVector3 *pUp);
