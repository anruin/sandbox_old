//
// Copyright (c) E. A. Pristavka, 2017
//

#ifndef SANDBOX_VECTOR3_H
#define SANDBOX_VECTOR3_H

#include <stddef.h>
#include "geometry.h"

#ifdef __cplusplus
extern "C" { // Do not allow C++ compiler to mangle API functions
#endif

typedef struct CVector3 {
    CReal x;
    CReal y;
    CReal z;
} CVector3;

CVector3 *CVector3_New(CVector3 *pSource);
CVector3 *CVector3_Set(CVector3 *pDest, const CVector3 *pSource);
CVector3 *CVector3_Fill(CVector3 *pDest, const CReal fX, const CReal fY, const CReal fZ);
CVector3 *CVector3_Add(CVector3 *pOut, const CVector3 *pFirst, const CVector3 *pSecond);
CVector3 *CVector3_Subtract(CVector3 *pOut, const CVector3 *pFirst, const CVector3 *pSecond);
CVector3 *CVector3_Multiply(CVector3 *pOut, const CVector3 *pFirst, const CVector3 *pSecond);
CReal CVector3_LengthSquared(const CVector3 *pVector);
CReal CVector3_Length(const CVector3 *pVector);
CVector3 *CVector3_Lerp(const CVector3 *pFrom, const CVector3 *pTo, CReal delta, CVector3 *pOut);
CVector3 *CVector3_Normalize(CVector3 *pDest, const CVector3 *pSource);

#ifdef __cplusplus
};
#endif

#endif //SANDBOX_VECTOR3_H
