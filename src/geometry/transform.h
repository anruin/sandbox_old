//
// Copyright (c) E. A. Pristavka, 2017
//

#ifndef SANDBOX_TRANSFORM_H
#define SANDBOX_TRANSFORM_H

#include "matrix4.h"
#include "vector3.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct CTransform {
    struct CMatrix4 pMatrix;
    struct CVector3 pPosition;
    struct CVector3 pRotation;
    struct CVector3 pScale;
} CTransform;

CTransform *CTransform_New(CTransform *pSource);
CTransform *CTransform_Set(CTransform *pDest, const CTransform *pSource);
CTransform *CTransform_Fill(CTransform *pDest, const CVector3 *pPosition, const CVector3 *pRotation, const CVector3 *pScale);
CTransform *CTransform_SetPosition(CTransform *pDest, const CVector3 *pPosition);
CTransform *CTransform_SetRotation(CTransform *pDest, const CVector3 *pRotation);
CTransform *CTransform_SetScale(CTransform *pDest, const CVector3 *pScale);

#ifdef __cplusplus
};
#endif

#endif //SANDBOX_TRANSFORM_H
