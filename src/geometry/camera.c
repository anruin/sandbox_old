//
// Copyright (c) E. A. Pristavka, 2017
//

#include <stdlib.h>
#include "camera.h"

CCamera *CCamera_New(CCamera* pSource) {
    CCamera *pCamera = calloc(1, sizeof(CCamera));

    if (pSource) {
        pCamera->transform = *CTransform_Set(&pCamera->transform, &pSource->transform);
    } else {
        pCamera->target = *CVector3_New(0);
        pCamera->target = *CVector3_Fill(&pCamera->up, 0, 0, 1);

        pCamera->up = *CVector3_New(0);
        pCamera->up = *CVector3_Fill(&pCamera->up, 0, 1, 0);
    }

    return pCamera;
}

CCamera *CCamera_SetTarget(CCamera *pDest, CVector3 *pTarget) {
    pDest->target = *CVector3_Set(&pDest->target, pTarget);
    CVector3_Normalize(&pDest->target, &pDest->target);
    return pDest;
}

CCamera *CCamera_SetUp(CCamera *pDest, CVector3 *pUp) {

}