//
// Copyright (c) E. A. Pristavka, 2017
//

#include "vector3.h"
#include <math.h>
#include <stdlib.h>
#include <memory.h>

CVector3 *CVector3_New(CVector3 *pSource) {
    CVector3 *pVector = calloc(1, sizeof(CVector3));

    if (pSource)
        memcpy(pVector, pSource, sizeof(CVector3));

    return pVector;
}

CVector3 *CVector3_Set(CVector3 *pDest, const CVector3 *pSource) {
    pDest->x = pSource->x;
    pDest->y = pSource->y;
    pDest->z = pSource->z;

    return pDest;
}

CVector3 *CVector3_Fill(CVector3 *pDest, const CReal fX, const CReal fY, const CReal fZ) {
    pDest->x = fX;
    pDest->y = fY;
    pDest->z = fZ;

    return pDest;
}

CVector3 *CVector3_Add(CVector3 *pOut, const CVector3 *pFirst, const CVector3 *pSecond) {
    pOut->x = pFirst->x + pSecond->x;
    pOut->y = pFirst->y + pSecond->y;
    pOut->z = pFirst->z + pSecond->z;

    return pOut;
}

CVector3 *CVector3_Subtract(CVector3 *pOut, const CVector3 *pFirst, const CVector3 *pSecond) {
    return NULL;
}

CVector3 *CVector3_Multiply(CVector3 *pOut, const CVector3 *pFirst, const CVector3 *pSecond) {
    return NULL;
}

CReal CVector3_Length(const CVector3 *pVector) {
    return sqrt_r(pVector->x * pVector->x
                + pVector->y * pVector->y
                + pVector->z * pVector->z);
}

CReal CVector3_LengthSquared(const CVector3 *pVector) {
    return pVector->x * pVector->x
           + pVector->y * pVector->y
           + pVector->z * pVector->z;
}

CVector3 *CVector3_Normalize(CVector3 *pDest, const CVector3 *pSource) {
    CReal length = CVector3_Length(pSource);

    pDest->x = pSource->x / length;
    pDest->y = pSource->y / length;
    pDest->z = pSource->z / length;

    return pDest;
}