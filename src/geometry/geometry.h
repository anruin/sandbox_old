//
// Copyright (c) E. A. Pristavka, 2017
//

#ifndef SANDBOX_GEOMETRY_H
#define SANDBOX_GEOMETRY_H

#include <math.h>

#ifdef __cplusplus
extern "C" { // Do not allow C++ compiler to mangle API functions
#endif

#ifdef SANDBOX_GEOMETRY_DOUBLE_PRECISION
#define CReal double
#define sqrt_r(x) sqrt(x)
#define cos_r(x) cos(x)
#define sin_r(x) sin(x)
#else
#define CReal float
#define sqrt_r(x) sqrtf(x)
#define cos_r(x) cosf(x)
#define sin_r(x) sinf(x)
#endif

void CReal_Print(CReal pSource);

struct CVector3;
void CVector3_Print(struct CVector3 *pSource);

struct CMatrix4;
void CMatrix4_Print(struct CMatrix4 *pSource);

struct CTransform;
void CTransform_Print(struct CTransform *pSource);

#ifdef __cplusplus
};
#endif

#endif //SANDBOX_GEOMETRY_H
