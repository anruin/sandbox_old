//
// Copyright (c) E. A. Pristavka, 2017
//

#include <stdio.h>
#include "geometry.h"
#include "vector3.h"
#include "matrix4.h"
#include "transform.h"

void CReal_Print(CReal pSource) {
    fprintf(stderr, "%.3lf\n", pSource);
}

void CVector3_Print(CVector3 *pSource) {
    fprintf(stderr, "%.3lf %.3lf %.3lf\n", pSource->x, pSource->y, pSource->z);
}

void CMatrix4_Print(struct CMatrix4 *pSource) {
    fprintf(stderr, "%.3lf %.3lf %.3lf %.3lf\n"
                    "%.3lf %.3lf %.3lf %.3lf\n"
                    "%.3lf %.3lf %.3lf %.3lf\n"
                    "%.3lf %.3lf %.3lf %.3lf\n",
            pSource->matrix[0], pSource->matrix[4], pSource->matrix[8], pSource->matrix[12],
            pSource->matrix[1], pSource->matrix[5], pSource->matrix[9], pSource->matrix[13],
            pSource->matrix[2], pSource->matrix[6], pSource->matrix[10], pSource->matrix[14],
            pSource->matrix[3], pSource->matrix[7], pSource->matrix[11], pSource->matrix[15]);
}

void CTransform_Print(struct CTransform *pSource) {
    fprintf(stderr, "P: %.3lf %.3lf %.3lf\n"
            "R: %.3lf %.3lf %.3lf\n"
            "S: %.3lf %.3lf %.3lf\n",
            pSource->pPosition.x, pSource->pPosition.y, pSource->pPosition.z,
            pSource->pRotation.x, pSource->pRotation.y, pSource->pRotation.z,
            pSource->pScale.x, pSource->pScale.y, pSource->pScale.z
    );
    CMatrix4_Print(&pSource->pMatrix);
}