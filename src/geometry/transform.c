//
// Copyright (c) E. A. Pristavka, 2017
//

#include <stdlib.h>
#include "transform.h"

CTransform *CTransform_New(CTransform *pSource) {

    CTransform *pTransform = calloc(1, sizeof(CTransform));

    pTransform->pPosition = *CVector3_New(NULL);
    pTransform->pRotation = *CVector3_New(NULL);
    pTransform->pScale = *CVector3_New(NULL);
    pTransform->pMatrix = *CMatrix4_New(NULL);

    if (pSource) {
        pTransform->pPosition = *CVector3_Set(&pTransform->pPosition, &pSource->pPosition);
        pTransform->pRotation = *CVector3_Set(&pTransform->pRotation, &pSource->pRotation);
        pTransform->pScale = *CVector3_Set(&pTransform->pScale, &pSource->pScale);
        pTransform->pMatrix = *CMatrix4_Set(&pTransform->pMatrix, &pSource->pMatrix);
    } else {
        pTransform->pMatrix = *CMatrix4_Identity(&pTransform->pMatrix);
    }

    return pTransform;
}

CTransform *CTransform_Set(CTransform *pDest, const CTransform *pSource) {

    if (!pSource)
        return NULL;

    if (!pDest)
        pDest = CTransform_New(NULL);

    pDest->pPosition = *CVector3_Set(&pDest->pPosition, &pSource->pPosition);
    pDest->pRotation = *CVector3_Set(&pDest->pRotation, &pSource->pRotation);
    pDest->pScale = *CVector3_Set(&pDest->pScale, &pSource->pScale);
    pDest->pMatrix = *CMatrix4_Set(&pDest->pMatrix, &pSource->pMatrix);

    return pDest;

}

CTransform *CTransform_SetPosition(CTransform *pDest, const CVector3 *pPosition) {
    pDest->pPosition = *CVector3_Set(&pDest->pPosition, pPosition);

    CMatrix4 *pTemp = CMatrix4_New(0);
    pTemp = CMatrix4_Translation(pTemp, pPosition);
    pDest->pMatrix = *CMatrix4_Multiply(&pDest->pMatrix, &pDest->pMatrix, pTemp);
    pTemp = CMatrix4_Delete(pTemp);

    return pDest;
}

CTransform *CTransform_SetRotation(CTransform *pDest, const CVector3 *pRotation) {
    pDest->pRotation = *CVector3_Set(&pDest->pPosition, pRotation);

    CMatrix4 *pTempX = CMatrix4_New(0);
    pTempX = CMatrix4_RotationX(pTempX, pRotation->x);

    CMatrix4 *pTempY = CMatrix4_New(0);
    pTempY = CMatrix4_RotationY(pTempY, pRotation->y);

    CMatrix4 *pTempZ = CMatrix4_New(0);
    pTempZ = CMatrix4_RotationZ(pTempZ, pRotation->z);

    pDest->pMatrix = *CMatrix4_Multiply((CMatrix4*)pDest, (CMatrix4*)pDest, pTempZ);
    pDest->pMatrix = *CMatrix4_Multiply((CMatrix4*)pDest, (CMatrix4*)pDest, pTempY);
    pDest->pMatrix = *CMatrix4_Multiply((CMatrix4*)pDest, (CMatrix4*)pDest, pTempX);

    return pDest;
}

CTransform *CTransform_SetScale(CTransform *pDest, const CVector3 *pScale) {
    pDest->pScale = *CVector3_Set(&pDest->pPosition, pScale);

    CMatrix4 *pTemp = CMatrix4_New(0);
    pTemp = CMatrix4_Scale(pTemp, pScale);

    pDest->pMatrix = *CMatrix4_Multiply((CMatrix4*)pDest, (CMatrix4*)pDest, pTemp);

    return pDest;
}

CTransform *CTransform_Fill(CTransform *pDest, const CVector3 *pPosition, const CVector3 *pRotation, const CVector3 *pScale) {

    CMatrix4 *pTemp = CMatrix4_New(0);
    pTemp = CMatrix4_Identity(pTemp);

    CMatrix4 *pScaleMatrix = CMatrix4_New(0);
    pScaleMatrix = CMatrix4_Scale(pScaleMatrix, pScale);

    CMatrix4 *pRotationMatrixZ = CMatrix4_New(0);
    CMatrix4 *pRotationMatrixY = CMatrix4_New(0);
    CMatrix4 *pRotationMatrixX = CMatrix4_New(0);
    CMatrix4 *pRotationMatrix = CMatrix4_New(0);

    pRotationMatrixZ = CMatrix4_RotationZ(pRotationMatrixZ, pRotation->z);
    pRotationMatrixY = CMatrix4_RotationY(pRotationMatrixY, pRotation->y);
    pRotationMatrixX = CMatrix4_RotationX(pRotationMatrixX, pRotation->x);

    CMatrix4_Multiply(pRotationMatrix, pRotationMatrixZ, pRotationMatrixY);
    CMatrix4_Multiply(pRotationMatrix, pRotationMatrix, pRotationMatrixX);

    CMatrix4 *pPositionMatrix = CMatrix4_New(0);
    pPositionMatrix = CMatrix4_Translation(pPositionMatrix, pPosition);

    pTemp = CMatrix4_Multiply(pTemp, pTemp, pScaleMatrix);
    pTemp = CMatrix4_Multiply(pTemp, pTemp, pRotationMatrix);
    pTemp = CMatrix4_Multiply(pTemp, pTemp, pPositionMatrix);

    pDest->pMatrix = *CMatrix4_Multiply(&pDest->pMatrix, &pDest->pMatrix, pTemp);

    pDest = CTransform_SetScale(pDest, pScale);
    pDest = CTransform_SetRotation(pDest, pRotation);
    pDest = CTransform_SetPosition(pDest, pPosition);

    return pDest;
}