//
// Copyright (c) E. A. Pristavka, 2017
//

#ifndef SANDBOX_MATRIX4_H
#define SANDBOX_MATRIX4_H

#include "geometry.h"

#ifdef __cplusplus
extern "C" {
#endif

struct CVector3;

typedef struct CMatrix4 {
    CReal matrix[16];
} CMatrix4;

CMatrix4 *CMatrix4_New(CMatrix4 *pSource);
CMatrix4 *CMatrix4_Delete(CMatrix4 *pSource);
CMatrix4 *CMatrix4_Set(CMatrix4 *pDest, const CMatrix4 *pSource);
CMatrix4 *CMatrix4_Identity(CMatrix4 *pDest);
CMatrix4 *CMatrix4_Inverse(CMatrix4 *pDest, const CMatrix4 *pSource);
CMatrix4 *CMatrix4_Transpose(CMatrix4 *pDest, const CMatrix4 *pSource);
CMatrix4 *CMatrix4_Multiply(CMatrix4 *pDest, const CMatrix4 *pFirst, const CMatrix4 *pSecond);
CMatrix4 *CMatrix4_Translation(CMatrix4 *pDest, const struct CVector3 *pSource);
CMatrix4 *CMatrix4_RotationX(CMatrix4 *pDest, const CReal fRadians);
CMatrix4 *CMatrix4_RotationY(CMatrix4 *pDest, const CReal fRadians);
CMatrix4 *CMatrix4_RotationZ(CMatrix4 *pDest, const CReal fRadians);
CMatrix4 *CMatrix4_Scale(CMatrix4 *pDest, const struct CVector3 *pSource);
CMatrix4 *CMatrix4_Perspective(CMatrix4 *pDest, CReal fFOV, CReal fAspect, CReal fNearClip, CReal fFarClip);
CMatrix4 *CMatrix4_Orthographic(CMatrix4 *pDest, CReal fLeft, CReal fRight, CReal fBottom, CReal fTop, CReal fNearClip, CReal fFarClip);
CMatrix4 *CMatrix4_LookAt(CMatrix4 *pDest, const struct CVector3 *pCamera, const struct CVector3 *pTarget, const struct CVector3 *pUp);

#ifdef __cplusplus
};
#endif

#endif //SANDBOX_MATRIX4_H
