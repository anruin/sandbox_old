//
// Copyright (c) E. A. Pristavka, 2017
//

#ifndef SANDBOX_PROJECTION_H
#define SANDBOX_PROJECTION_H

#include "geometry.h"

struct CPerspectiveProjection {
    CReal fFOV;
    CReal fWidth;
    CReal fHeight;
    CReal fNearClip;
    CReal fFarClip;
};

struct COrthogonalProjection {
    CReal fTop;
    CReal fRight;
    CReal fBottom;
    CReal fLeft;
    CReal fNear;
    CReal fFar;
};

#endif //SANDBOX_PROJECTION_H
