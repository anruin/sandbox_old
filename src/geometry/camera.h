//
// Copyright (c) E. A. Pristavka, 2017
//

#ifndef SANDBOX_CAMERA_H
#define SANDBOX_CAMERA_H

#include "transform.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct CCamera {
    CTransform transform;
    CVector3 target;
    CVector3 up;
    int width;
    int height;
} CCamera;

CCamera *CCamera_New(CCamera *pSource);
CCamera *CCamera_SetTarget(CCamera *pDest, CVector3 *pTarget);
CCamera *CCamera_SetUp(CCamera *pDest, CVector3 *pUp);

#ifdef __cplusplus
};
#endif

#endif //SANDBOX_CAMERA_H
