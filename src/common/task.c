// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

//
// Created by Egor Pristavka on 07.09.17.
//

#include "api/platform.h"
#include "api/application.h"
#include "common/common.h"
#include "common/task.h"
#include "common/vector.h"
#include "thread.h"

#ifdef _WIN32
#include <windows.h>
#include <process.h>
#else
#include <pthread.h>
#endif

static CVector *ATaskManager_WorkerThreads;
static CVector *ATaskManager_Tasks;
static CVector *ATaskManager_CompleteTaskList;
static CBool ATaskManager_Running = 1;

#ifdef _WIN32
static HANDLE ATaskManager_TaskContainerMutex;
#else
static pthread_mutex_t ATaskManager_TaskContainerMutex;
#endif

ATask *ATask_New() {
	ATask *pTask = calloc(1, sizeof(ATask));
	return pTask;
}

void ATask_Delete(ATask *pTask) {
	FREE(pTask);
}

void DTask_ExampleTask(void *p) {
	ATask *pTask = (ATask *)p;
#ifdef _WIN32
	DEBUG_PRINT("Example task %d thread %zd\n", pTask->nId, (size_t)GetCurrentThreadId());
#else
    DEBUG_PRINT("Example task %d thread %zd\n", pTask->nId, (size_t)pthread_self());
#endif
}

void DTask_PrintThreadIds() {
	// Print thread ids
	for (int32_t i = 0; i < ATaskManager_WorkerThreads->nCount; i++) {
		AThread *pThread;
		CVector_GetPointer(ATaskManager_WorkerThreads, i, pThread);
#ifdef _WIN32
		DEBUG_PRINT("Thread ID: %i\n", (int)GetThreadId(pThread->hThread));
#else
        DEBUG_PRINT("Thread ID: %i\n", (int)pthread_self());
#endif
	}
}

void DTask_CreateTasks() {
	for (int i = 0; i < 1000; i++) {
		ATask *pTask = ATask_New();
		if (pTask) {
			pTask->pfnOnProcess = DTask_ExampleTask;
			pTask->nId = (int)ATaskManager_Tasks->nCount;
			CVector_PushBack(ATaskManager_Tasks, pTask, sizeof(ATask));
		} else {
			DEBUG_PRINT("Error creating task!\n");
		}
	}
}

void ATaskManager_OnCreate() {

	// Initialize tasks array.
	ATaskManager_Tasks = CVector_New(CVector_DefaultCapacity, CVector_DefaultDestructor);

	// Initialize worker threads array.
	ATaskManager_WorkerThreads = CVector_New(CVector_DefaultCapacity, CVector_DefaultDestructor);

	// Create tasks mutex which will be used in worker threads to access task list.
#if defined(_WIN32)
	ATaskManager_TaskContainerMutex = CreateMutexA(NULL, FALSE, NULL);
	if (ATaskManager_TaskContainerMutex == NULL) {
		DEBUG_PRINT("Create mutex failed: %i\n", (int32_t)GetLastError());
		return;
	}
#else
    int32_t nResult = pthread_mutex_init(&ATaskManager_TaskContainerMutex, NULL);
	if (nResult != 0) {
		DEBUG_PRINT("Error initializing mutex: %i\n", nResult);
		return;
	}
#endif

	// Get threads count, by default let us set it to processor count.
	uint32_t nThreadCount = APlatform_GetProcessorCount();

	// Create threads.
	for (uint32_t i = 0; i < nThreadCount; i++) {
		AThread *pThread = AThread_New(ATaskManager_WorkerThreadFunction);
		CVector_PushBack(ATaskManager_WorkerThreads, pThread, sizeof(AThread));
		AThread_Delete(pThread);
	}
}

void ATaskManager_KillThread(void *pThread) {
	// Kill pthread.
#if defined(_WIN32)
	WaitForSingleObject(CAST(pThread, AThread *)->hThread, INFINITE);
#else
	pthread_cancel(((AThread *)pThread)->pThread);
#endif
}

void ATaskManager_OnDestroy() {
	ATaskManager_Running = 0;

	// Close all handles, stop threads, delete mutex
	CVector_Each(ATaskManager_WorkerThreads, ATaskManager_KillThread);

	CVector_Delete(ATaskManager_WorkerThreads);

#if defined(WIN32)
	CloseHandle(ATaskManager_TaskContainerMutex);
#else
	pthread_mutex_destroy(&ATaskManager_TaskContainerMutex);
#endif

	CVector_Delete(ATaskManager_Tasks);
}

void *ATaskManager_WorkerThreadFunction(void *p) {

	AThread *pThread = (AThread *)p;

	ATask *pTask = NULL;

	while (ATaskManager_Running) {
		// Get new task.
#ifdef _WIN32
		// Acquire lock
		int nWaitResult = WaitForSingleObject(ATaskManager_TaskContainerMutex, INFINITE);

		switch (nWaitResult) {
			case WAIT_OBJECT_0:
				if (ATaskManager_Tasks->nCount > 0) {
					CError nError = AVector_PopFront(ATaskManager_Tasks, &pTask);
					if (nError != CERROR_OK) {
						DEBUG_PRINT("Error retrieving task %d\n", nError);
						return NULL;
					}
				}
				break;
			case WAIT_ABANDONED:
				DEBUG_PRINT("Error locking mutex: thread abandoned!\n");
				return NULL;
		}

		// Release lock
		if (!ReleaseMutex(ATaskManager_TaskContainerMutex)) {
			DEBUG_PRINT("Error releasing mutex %i from thread %i\n", (int)GetLastError(), pThread->nThreadId);
			return NULL;
		}
#else
		// Acquire lock
        int nResult = pthread_mutex_lock(&ATaskManager_TaskContainerMutex);

		if (nResult != 0) {
			DEBUG_PRINT("Error locking mutex: %i\n", nResult);
            return NULL;
		}

        if (ATaskManager_Tasks->nCount > 0) {
            CError nError = CVector_PopFront(ATaskManager_Tasks, (void **) &pTask);
            if (nError != CERROR_OK) {
                DEBUG_PRINT("Error retrieving task %d\n", nError);
                return NULL;
            }
        }

		nResult = pthread_mutex_unlock(&ATaskManager_TaskContainerMutex);

		if (nResult != 0) {
			DEBUG_PRINT("Error unlocking mutex: %i\n", nResult);
			return NULL;
		}
#endif
		// Execute current task.
		if (pTask) {
			pTask->pfnOnProcess(pTask);

			if (pTask->pfnOnComplete != NULL) {
				pTask->pfnOnComplete(pTask->pData);
			}
		}
	}

	return NULL;
}

void ATaskManager_EnqueueTask(ATask *pTask) {
	CVector_PushBack(ATaskManager_Tasks, pTask, sizeof(ATask));
}
