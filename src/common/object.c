// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
//
// Copyright (c) E. A. Pristavka, 2017
//

#include "object.h"
#include <memory.h>

static int32_t AObject_LastId = 0;

CObject *CObject_New(void *pSource, int32_t nSize) {
    CObject *pObject = calloc(1, sizeof(CObject));

    if (!pObject)
        return NULL;

    pObject->size = nSize;
    pObject->pData = calloc(1, SIZE(nSize));
	pObject->id = AObject_LastId++;

    if (!pObject->pData) {
        FREE(pObject);
        return NULL;
    }

    memcpy(pObject->pData, pSource, SIZE(nSize));

    return pObject;
}

CError CObject_Get(CObject *pObject, void **ppDestination) {
    *ppDestination = calloc(1, SIZE(pObject->size));

    if (!*ppDestination)
        return CERROR_FAIL;

    memcpy(*ppDestination, pObject->pData, SIZE(pObject->size));

    return CERROR_OK;
}

void CObject_Delete(CObject *pObject) {
    if (pObject) {
        FREE(pObject->pData);
        FREE(pObject);
    }
}
