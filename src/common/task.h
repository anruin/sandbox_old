//
// Created by Egor Pristavka on 07.09.17.
//

#pragma once

#include "common/common.h"

/**
 * @struct ATask
 * @property ACallbackFunc pfnProcess Task processing function.
 * @property ACallbackFunc pfnComplete Task complete callback function.
 * @property void *pData Task data, which will be passed to complete callback.
 * @property int32_t nId Task id.
 */
typedef struct ATask {
	CCallbackFunc pfnOnProcess;
	CCallbackFunc pfnOnComplete;
	void *pData;
	int32_t nId;
} ATask;

ATask *ATask_New();
void ATask_Delete(ATask *pTask);

/**
 * @fn void *ATaskManager_WorkerThreadFunction(void *p)
 * @brief Main function of worker thread.
 * @details Waits for a new task and then processes it.
 */
void *ATaskManager_WorkerThreadFunction(void *p);

/**
 * @fn void ATaskManager_OnCreate()
 * @brief Called on application start to initialize functionality related to task processing.
 */
void ATaskManager_OnCreate();

/**
 * @fn void ATaskManager_OnDestroy()
 * @brief Called on application exit to clean up memory related to task processing.
 */
void ATaskManager_OnDestroy();

/**
 * @fn void ATaskManager_EnqueueTask(ATask *pTask)
 * @brief Pushes task to the task queue.
 * @param pTask
 */
void ATaskManager_EnqueueTask(ATask *pTask);