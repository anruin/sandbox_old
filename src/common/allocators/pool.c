//
// Created by Egor Pristavka on 08.02.2018.
//

#include "pool.h"

static void APoolAllocator_Reset(APoolAllocator *pAllocator);

void APoolAllocator_OnCreate(APoolAllocator *pAllocator, const u32 nCount, const u32 nSize) {
    assert(nSize >= sizeof(void *) && "Size of chunk is too small");

    pAllocator->freeList.pHead = NULL;

    pAllocator->nUsed = 0;

    pAllocator->nSize = nSize;
    pAllocator->nCount = nCount;

    pAllocator->pStart = calloc(nCount, nSize);

    APoolAllocator_Reset(pAllocator);
}

void APoolAllocator_Destroy(APoolAllocator *pAllocator) {
    free(pAllocator->pStart);
}

void *APoolAllocator_Allocate(APoolAllocator *pAllocator) {
    AListNode *pFree = AList_Pop(&pAllocator->freeList);

    assert(pFree != NULL && "The allocator pool is full");

    pAllocator->nUsed += pAllocator->nSize;

    return pFree->pData;
}

void APoolAllocator_Free(APoolAllocator *pAllocator, void *p) {
    pAllocator->nUsed -= pAllocator->nSize;
    AList_Push(&pAllocator->freeList, (AListNode *) p);
}

static void APoolAllocator_Reset(APoolAllocator *pAllocator) {
    pAllocator->nUsed = 0;

    for (u32 i = 0; i < pAllocator->nCount; i++) {
        void *p = pAllocator->pStart + (i * pAllocator->nSize) + sizeof(void *);

        AList_Push(&pAllocator->freeList, (AListNode *) p);
    }
}