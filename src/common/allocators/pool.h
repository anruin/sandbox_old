//
// Created by Egor Pristavka on 08.02.2018.
//

#ifndef SANDBOX_POOL_ALLOCATOR_H
#define SANDBOX_POOL_ALLOCATOR_H

#include <common/common.h>
#include <common/containers/list.h>

typedef struct APoolAllocator {
    AList freeList;
    void *pStart;

    u32 nSize;
    u32 nCount;
    u32 nUsed;
} APoolAllocator;

extern void APoolAllocator_OnCreate(APoolAllocator *pAllocator, u32 nCount, u32 nSize);

extern void APoolAllocator_Destroy(APoolAllocator *pAllocator);

extern void *APoolAllocator_Allocate(APoolAllocator *pAllocator);

extern void APoolAllocator_Free(APoolAllocator *pAllocator, void *p);

#endif //SANDBOX_POOL_ALLOCATOR_H
