//
// Copyright (c) E. A. Pristavka, 2017
//

#pragma once

#include "common/object.h"

typedef struct AEvent {
    AString *sTopic;
    CObject *pData;
} AEvent;

extern AEvent *AEvent_New(const struct AString* sTopic, void *pData, int32_t nDataSize);
extern void AEvent_Delete(AEvent *pEvent);
