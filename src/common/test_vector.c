// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include <string.h>
#include "common/common.h"
#include "vector.h"
#include <assert.h>
#if defined(_WIN32) && defined(_MSC_VER)
#include <vld.h>
#endif

void TestVectorLeaks() {

	CVector *pVector = CVector_New(CVector_DefaultCapacity, CVector_DefaultDestructor);

	// Insert string into array.
	CVector_PushBackCString(pVector, "Example");

	// Insert integer value into array.
	CVector_PushBackPrimitive(pVector, int32_t, 0x00524745);

	char *pString;
	CVector_GetPointer(pVector, 0, pString);

	int32_t value;
	CVector_GetPrimitive(pVector, 1, int32_t, value);

	DEBUG_PRINT("%s\n%x\n", pString, value);

	FREE(pString);

	CVector_Delete(pVector);
}

void TestVectorNew() {
	CVector *pVector = CVector_NewDefault();

	CVector_Delete(pVector);
}

void TestVectorSet() {
	CVector *pVector = CVector_NewDefault();

	int32_t value = 0x00524745;
	int32_t value2 = 0x00524745;

	CVector_Set(pVector, 0, &value, sizeof(int32_t));

	CVector_Set(pVector, 8, &value2, sizeof(int32_t));

	CVector_Delete(pVector);
}

void TestVectorInsert() {
	CVector *pVector = CVector_NewDefault();

	int32_t value = 0x00524745;

	CVector_Insert(pVector, 0, &value, sizeof(int32_t));

	CVector_Insert(pVector, 8, &value, sizeof(int32_t));

	CVector_Delete(pVector);
}

void TestVectorPushBack() {
	CVector *pVector = CVector_NewDefault();

	int32_t value = 0x00524745;

	CVector_PushBack(pVector, &value, sizeof(int32_t));

	CVector_Delete(pVector);
}

void TestVectorGet() {
	CVector *pVector = CVector_NewDefault();
	
	int32_t value = 0x00524745;

	CVector_Set(pVector, 0, &value, sizeof(int32_t));

	int32_t *pValue = NULL;

	CVector_Get(pVector, 0, &pValue);

	assert(*pValue == value);

	FREE(pValue);
}

void TestVectorPop() {
	CVector *pVector = CVector_NewDefault();

	int32_t value = 0x00524745;

	CVector_PushBack(pVector, &value, sizeof(int32_t));
	CVector_PushBack(pVector, &value, sizeof(int32_t));

	int32_t *pValue = NULL;

	CVector_PopFront(pVector, &pValue);

	assert(*pValue == value);

	FREE(pValue);

	CVector_PopBack(pVector, &pValue);

	assert(*pValue == value);

	FREE(pValue);

	CVector_Delete(pVector);
}

void TestVectorEmptyClear() {

	CVector *pVector = CVector_NewDefault();

	assert(CVector_Empty(pVector));

	int32_t value = 0x00524745;

	CVector_PushBack(pVector, &value, sizeof(int32_t));

	assert(!CVector_Empty(pVector));

	CVector_Clear(pVector);

	assert(CVector_Empty(pVector));

	CVector_Delete(pVector);
}

int main() {
	
	TestVectorNew();

	TestVectorSet();

	TestVectorInsert();

	TestVectorPushBack();

	TestVectorGet();

	TestVectorPop();

	TestVectorEmptyClear();

	TestVectorLeaks();

	return 0;
}