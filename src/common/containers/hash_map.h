//
// Created by Egor Pristavka on 22.02.2018.
//

#ifndef SANDBOX_HASH_MAP_H
#define SANDBOX_HASH_MAP_H

#include <vendor/khash.h>
#include <common/string.h>

KHASH_MAP_INIT_STR(CString, char *);

typedef khash_t(CString) AStringHashMap;
typedef khiter_t AStringHashMapIterator;
typedef void(*AStringHashMapCallbackFn)(CString pKey, CString pValue);

#define AStringHashMap_Create() kh_init(CString);
#define AStringHashMap_Destroy(pMap) kh_destroy(CString, pMap);
#define AStringHashMap_Set(pMap, pKey, pValue) kh_put(CString, pMap, pKey, pValue);
#define AStringHashMap_Get(pMap, pKey) kh_get_val(CString, pMap, pKey, NULL);
#define AStringHashMap_Each(pMap, pKey, pValue, pfnCallback) kh_foreach(pMap, pKey, pValue, pfnCallback(pKey, pValue));

inline void AStringHashMap_Delete(AStringHashMap *pMap, CString pKey) {
    AStringHashMapIterator iterator = kh_get(CString, pMap, pKey);
    if (iterator != kh_end(pMap))
        kh_del(CString, pMap, iterator);
}

#endif //SANDBOX_HASH_MAP_H
