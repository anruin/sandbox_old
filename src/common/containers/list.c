//
// Created by Egor Pristavka on 08.02.2018.
//

#include "list.h"

extern void AList_Push(AList *pList, AListNode *pNode) {
    pNode->pNext = pList->pHead;
    pList->pHead = pNode;
}

extern AListNode *AList_Pop(AList *pList) {
    AListNode *pHead = pList->pHead;
    pList->pHead = pHead->pNext;
    return pHead;
}