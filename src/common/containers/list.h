//
// Created by Egor Pristavka on 08.02.2018.
//

#ifndef SANDBOX_LIST_H
#define SANDBOX_LIST_H

#include <common/object.h>

typedef struct AList AList;
typedef struct AListNode AListNode;

struct AListNode {
    AListNode *pNext;
    void *pData;
};

struct AList {
    AListNode *pHead;
};

extern void AList_Push(AList *pList, AListNode *pNode);

extern AListNode *AList_Pop(AList *pList);

#endif //SANDBOX_LIST_H
