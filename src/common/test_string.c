// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

#include "common/common.h"
#include "string.h"

int main() {
	AString *pString = AString_New("EXAMPLE");
	
	AString_Append(pString, " 8 MORE");

	DEBUG_PRINT("%s", pString->pString);

	return 0;
}