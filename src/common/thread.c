// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

//
// Created by Egor Pristavka on 07.09.17.
//

#include "common/common.h"
#include "common/thread.h"

#ifdef _WIN32
#include <windows.h>
#include <process.h>
#else
#include <pthread.h>
#endif

AThread *AThread_New(CThreadFunc pfnWorker) {
    AThread *pThread = calloc(1, sizeof(AThread));

	if (!pThread)
		return NULL;

    pThread->pfnWorker = pfnWorker;
#ifdef _WIN32
    pThread->hThread = (HANDLE)_beginthreadex(NULL, 0, (_beginthreadex_proc_type)pThread->pfnWorker, pThread, 0, &pThread->nThreadId);
#else
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_create((pthread_t *)&pThread->pThread, &attr, pfnWorker, NULL);
#endif

    return pThread;
}

void AThread_Delete(AThread *pThread) {
    FREE(pThread);
}