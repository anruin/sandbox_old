// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com
//
// Copyright (c) E. A. Pristavka, 2017
//

#include <string.h>
#include "string.h"
#include "event.h"

AEvent *AEvent_New(const AString* sTopic, void *pData, int32_t nSize) {
    AEvent *pEvent = calloc(1, sizeof(AEvent));

    if (!pEvent)
        return NULL;

	pEvent->sTopic = (AString *)sTopic;

    if (pData && nSize) {
        pEvent->pData = CObject_New(pData, nSize);

        if (!pEvent->pData) {
            FREE(pEvent);
            return NULL;
        }
    } else {
        pEvent->pData = NULL;
    }

    return pEvent;
}

void AEvent_Delete(AEvent *pEvent) {
    if (pEvent) {
		FREE(pEvent->pData);
        FREE(pEvent);
    }
}
