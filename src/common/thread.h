//
// Created by Egor Pristavka on 07.09.17.
//

#pragma once

#include "common/common.h"

#ifdef _WIN32
#include <windows.h>
typedef struct AThread {
	HANDLE hThread;
	CThreadFunc pfnWorker;
	int32_t nThreadId;
} AThread;
#else
#include <pthread.h>
typedef struct AThread {
	pthread_t pThread;
	CThreadFunc pfnWorker;
	int32_t nThreadId;
} AThread;
#endif

AThread *AThread_New(CThreadFunc pfnWorker);
void AThread_Delete(AThread *pThread);
