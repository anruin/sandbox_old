//
// Copyright (c) E. A. Pristavka, 2017
//

#ifndef SANDBOX_MOPENGL_CAMERA_H
#define SANDBOX_MOPENGL_CAMERA_H


#include <geometry/vector3.h>

class MCamera {
public:
    MCamera(int iWindowWidth, int iWindowHeight);
    MCamera(int iWindowWidth, int iWindowHeight, const CVector3& fPosition, const CVector3& fTarget, const CVector3& fUp);

    void OnCreate();
    void OnUpdate();
    void OnRender();

private:
    CVector3 position;
    CVector3 target;
    CVector3 up;

    int windowWidth;
    int windowHeight;
};


#endif //SANDBOX_MOPENGL_CAMERA_H
