//
// Copyright (c) E. A. Pristavka, 2017
//

#ifndef SANDBOX_MOPENGL_PIPELINE_H
#define SANDBOX_MOPENGL_PIPELINE_H

#include <geometry/vector3.h>
#include <geometry/matrix4.h>
#include <geometry/projection.h>
#include "camera.hpp"

class MPipeline {

public:
    MPipeline() {

    }

    void Scale(CReal fScale) {
        Scale(fScale, fScale, fScale);
    }

    void Scale(CReal fScaleX, CReal fScaleY, CReal fScaleZ) {
        worldScale.x = fScaleX;
        worldScale.y = fScaleY;
        worldScale.z = fScaleZ;
    }

    void Scale(const CVector3& fScale) {
        Scale(fScale.x, fScale.y, fScale.z);
    }

private:
    CVector3 worldScale;
    CVector3 worldPosition;
    CVector3 worldRotation;

    MCamera *camera;

    CPerspectiveProjection perspectiveProjection;
    COrthogonalProjection orthogonalProjection;

    CMatrix4 worldViewProjection;
    CMatrix4 viewProjection;
    CMatrix4 worldProjection;
    CMatrix4 worldView;
    CMatrix4 world;
    CMatrix4 view;
    CMatrix4 projection;
};


#endif //SANDBOX_MOPENGL_PIPELINE_H
