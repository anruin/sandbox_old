//
// Copyright (c) E. A. Pristavka, 2017
//

#include "camera.hpp"

MCamera::MCamera(int iWindowWidth, int iWindowHeight) {
    windowWidth = iWindowWidth;
    windowHeight = iWindowHeight;

    position = *CVector3_New(NULL);

    target = *CVector3_New(NULL);
    target.z = 1;

    up = *CVector3_New(NULL);
    up.y = 1;

    OnCreate();
}

MCamera::MCamera(int iWindowWidth, int iWindowHeight, const CVector3 &fPosition, const CVector3 &fTarget, const CVector3 &fUp) {
    windowWidth = iWindowWidth;
    windowHeight = iWindowHeight;

    position = fPosition;

    target = fTarget;
    target.z = 1;

    up = fUp;
    up.y = 1;

    OnCreate();
}

void MCamera::OnCreate() {

}

void MCamera::OnRender() {

}

void MCamera::OnUpdate() {

}