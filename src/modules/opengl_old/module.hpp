//
// Copyright (c) E. A. Pristavka, 2017
//

#ifndef SANDBOX_OPENGL_MODULE_H
#define SANDBOX_OPENGL_MODULE_H

#include <SDL.h>
#include <api/module.h>

DECLARE_MODULE_INTERFACE()

class MOpenGL {
private:
    static const int nWindowWidth = 1024;
    static const int nWindowHeight = 768;

    SDL_Window *pWindow;
    SDL_GLContext hContext;

    CCamera *pCamera;

public:
    MOpenGL();

public:
    void OnCreate();
    void OnUpdate();
    void OnDestroy();
    void OnEvent(CEvent *pEvent);
    void OnReady();

private:
    void InitializeSDL();
    void InitializeOpenGL();
};

#endif //SANDBOX_OPENGL_MODULE_H
