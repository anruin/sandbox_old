//
// Copyright (c) E. A. Pristavka, 2017
//

#include <SDL.h>
#include <GL/glew.h>
#include <geometry/camera.h>
#include "module.hpp"

static MOpenGL Instance;

#ifdef __cplusplus
extern "C" { // Do not allow C++ compiler to mangle API functions
#endif

void MODULE_ON_CREATE() {

    Instance.OnCreate();

    DEBUG_PRINT("MOpenGL loaded.\n");

    DEBUG_PRINT("----------------------------------------------------------------\n");
    DEBUG_PRINT("OpenGL Info\n");
    DEBUG_PRINT("    Version: %s\n", glGetString(GL_VERSION));
    DEBUG_PRINT("     Vendor: %s\n", glGetString(GL_VENDOR));
    DEBUG_PRINT("   Renderer: %s\n", glGetString(GL_RENDERER));
    DEBUG_PRINT("    Shading: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    DEBUG_PRINT("----------------------------------------------------------------\n");

}

void MODULE_ON_READY() {
    Instance.OnReady();
}

void MODULE_ON_UPDATE() {
    Instance.OnUpdate();
}

void MODULE_ON_DESTROY() {
    Instance.OnDestroy();

    DEBUG_PRINT("MOpenGL unloaded.\n");
}

void *MODULE_CALL(const char *name, void **parameters) {
    return NULL;
}

void MODULE_ON_EVENT(CEvent *pEvent) {
    DEBUG_PRINT("MOpenGL received event %s.\n", pEvent->arrTopic);

    Instance.OnEvent(pEvent);
}

#ifdef __cplusplus
} // extern "C"
#endif

MOpenGL::MOpenGL() {
    pWindow = nullptr;
    hContext = nullptr;
    pCamera = nullptr;
}

void MOpenGL::OnCreate() {

    InitializeSDL();
    InitializeOpenGL();

    pCamera = CCamera_New(nullptr);
    pCamera->width = MOpenGL::nWindowWidth;
    pCamera->height = MOpenGL::nWindowHeight;

}

void MOpenGL::OnReady() {

}

void MOpenGL::OnUpdate() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    SDL_GL_SwapWindow(pWindow);

}

void MOpenGL::OnDestroy() {
    SDL_GL_DeleteContext(hContext);
    SDL_DestroyWindow(pWindow);

    SDL_QuitSubSystem(SDL_INIT_VIDEO);
}

void MOpenGL::OnEvent(CEvent *pEvent) {

}

void MOpenGL::InitializeSDL() {
    if (SDL_InitSubSystem(SDL_INIT_VIDEO) != 0) {
        DEBUG_PRINT("MOpenGL: Error initializing SDL: %s\n", SDL_GetError());
    }

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

    SDL_SetRelativeMouseMode(SDL_FALSE);

    pWindow = SDL_CreateWindow("Sandbox",
                               SDL_WINDOWPOS_UNDEFINED,
                               SDL_WINDOWPOS_UNDEFINED,
                               nWindowWidth,
                               nWindowHeight,
                               SDL_WINDOW_OPENGL);

    if (!pWindow) {
        DEBUG_PRINT("MOpenGL: Error creating SDL window: %s\n", SDL_GetError());
    }

    hContext = SDL_GL_CreateContext(pWindow);

    if (!hContext) {
        DEBUG_PRINT("MOpenGL: Error creating OpenGL context: %s\n", SDL_GetError());
    }

}

void MOpenGL::InitializeOpenGL() {
    glewExperimental = GL_TRUE;

    GLenum result = glewInit();

    if (result != GLEW_OK) {
        DEBUG_PRINT("MOpenGL: Error initializing GLEW! %s\n", glewGetErrorString(result));
    }

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
}
