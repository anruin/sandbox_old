cmake_minimum_required(VERSION 3.5)

set(OpenGL_GL_PREFERENCE GLVND)

# Third-party dependencies
find_package(OpenGL REQUIRED)
find_package(GLEW REQUIRED)
find_package(SDL2 REQUIRED)
find_package(SDL2_image REQUIRED)
find_package(SDL2_ttf REQUIRED)

include_directories(${OPENGL_INCLUDE_DIR} ${GLU_INCLUDE_DIR} ${GLEW_INCLUDE_DIRS}
        ${SDL2_INCLUDE_DIRS} ${SDL2_IMAGE_INCLUDE_DIR})

set(SOURCE_FILES module.c)

add_library(SandboxOpenGLModule MODULE ${SOURCE_FILES})

target_compile_definitions(SandboxOpenGLModule PRIVATE SANDBOX_MODULE)
set_property(TARGET SandboxOpenGLModule PROPERTY POSITION_INDEPENDENT_CODE ON)

if (WIN32)
    target_link_libraries(SandboxOpenGLModule PUBLIC
            ${OPENGL_LIBRARIES} ${GLEW_LIBRARIES}
            ${SDL2_LIBRARY} ${SDL2_IMAGE_LIBRARIES})
else ()
    target_link_libraries(SandboxOpenGLModule PUBLIC c
            ${OPENGL_LIBRARIES} ${GLEW_LIBRARIES}
            ${SDL2_LIBRARY} ${SDL2_IMAGE_LIBRARIES})
endif ()