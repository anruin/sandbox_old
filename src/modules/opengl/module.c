// This is an independent project of an individual developer. Dear PVS-Studio, please check it.
// PVS-Studio Static Code Analyzer for C, C++ and C#: http://www.viva64.com

//
// Copyright (c) E. A. Pristavka, 2017
//

#include <common/common.h>
#include <api/module.h>

#include <SDL.h>
#include <GL/glew.h>
#include <SDL_image.h>

CStringConst AModule_Name = "OpenGL";

static const int nWindowWidth = 1024;
static const int nWindowHeight = 768;
SDL_Window *pWindow;
SDL_GLContext pContext;

u32 nTextureId = 0;

static void MOpenGL_InitializeSDL();

static void MOpenGL_InitializeOpenGL();

u32 MOpenGL_LoadSDLTexture(CStringConst pPath);

void AModule_OnCreate() {

    MOpenGL_InitializeSDL();
    MOpenGL_InitializeOpenGL();

    DEBUG_PRINT("OpenGL module created!\n");

    DEBUG_PRINT("----------------------------------------------------------------\n");
    DEBUG_PRINT("OpenGL Info\n");
    DEBUG_PRINT("  Version: %s\n", glGetString(GL_VERSION));
    DEBUG_PRINT("  Vendor: %s\n", glGetString(GL_VENDOR));
    DEBUG_PRINT("  Renderer: %s\n", glGetString(GL_RENDERER));
    DEBUG_PRINT("  Shading: %s\n", glGetString(GL_SHADING_LANGUAGE_VERSION));
    DEBUG_PRINT("----------------------------------------------------------------\n");
}

void AModule_OnReady() {
    DEBUG_PRINT("Loading texture!\n");
    nTextureId = MOpenGL_LoadSDLTexture("./assets/textures/test.png");

    DEBUG_PRINT("OpenGL module ready!\n");
}

void AModule_OnUpdate() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glBindTexture(GL_TEXTURE_2D, nTextureId);

    // For Ortho mode, of course
    int X = 0;
    int Y = 0;
    int Width = 100;
    int Height = 100;

    glBegin(GL_QUADS);
    glTexCoord2f(0, 0); glVertex3f(X, Y, 0);
    glTexCoord2f(1, 0); glVertex3f(X + Width, Y, 0);
    glTexCoord2f(1, 1); glVertex3f(X + Width, Y + Height, 0);
    glTexCoord2f(0, 1); glVertex3f(X, Y + Height, 0);
    glEnd();

    SDL_GL_SwapWindow(pWindow);
}

void AModule_OnDestroy() {
    SDL_GL_DeleteContext(pContext);
    SDL_DestroyWindow(pWindow);

    SDL_QuitSubSystem(SDL_INIT_VIDEO);

    DEBUG_PRINT("OpenGL module unloaded!\n");
}

void MOpenGL_InitializeSDL() {
    if (SDL_InitSubSystem(SDL_INIT_VIDEO) != 0)
        DEBUG_PRINT("MOpenGL: Error initializing SDL: %s\n", SDL_GetError());

    SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
    SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24);

    SDL_SetRelativeMouseMode(SDL_FALSE);

    pWindow = SDL_CreateWindow("Sandbox OpenGL",
                               SDL_WINDOWPOS_UNDEFINED,
                               SDL_WINDOWPOS_UNDEFINED,
                               nWindowWidth,
                               nWindowHeight,
                               SDL_WINDOW_OPENGL);

    if (!pWindow)
        DEBUG_PRINT("MOpenGL: Error creating SDL window: %s\n", SDL_GetError());

    pContext = SDL_GL_CreateContext(pWindow);

    if (!pContext)
        DEBUG_PRINT("MOpenGL: Error creating OpenGL context: %s\n", SDL_GetError());

    SDL_ShowSimpleMessageBox(SDL_MESSAGEBOX_ERROR, "MOpenGL", "Test", NULL);
}

void MOpenGL_InitializeOpenGL() {
    glewExperimental = GL_TRUE;

    GLenum result = glewInit();

    if (result != GLEW_OK)
        DEBUG_PRINT("MOpenGL: Error initializing GLEW: %s\n", glewGetErrorString(result));

    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LESS);

    glEnable(GL_CULL_FACE);
    glCullFace(GL_BACK);
}

u32 MOpenGL_LoadSDLTexture(CStringConst pPath) {
    GLuint nTextureId = 0;

    SDL_Surface *pSurface = IMG_Load(pPath);

    if (!pSurface) {
        DEBUG_PRINT("Texture was not found: %s %s\n", pPath, SDL_GetError());
        return 0;
    }

    glGenTextures(1, &nTextureId);
    glBindTexture(GL_TEXTURE_2D, nTextureId);

    GLenum nMode = GL_RGB;

    if (pSurface->format->BytesPerPixel == 4) {
        nMode = GL_RGBA;
    }

    glTexImage2D(GL_TEXTURE_2D, 0, nMode, pSurface->w, pSurface->h, 0, nMode, GL_UNSIGNED_BYTE, pSurface->pixels);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    return nTextureId;
}