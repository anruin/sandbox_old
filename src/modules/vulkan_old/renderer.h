//
// Copyright (c) E. A. Pristavka, 2017
//

#ifndef SANDBOX_RENDERER_H
#define SANDBOX_RENDERER_H

#include <api.h>
#include <SDL_video.h>
#include <vulkan/vulkan.h>
#include <assert.h>
#include <vector.h>

typedef struct vk_renderer_queue_t {
    uint32_t iQueueFamily;
    VkQueue hQueue;
    VkCommandPool hCmdPool;
} vk_renderer_queue_t;

/**
 * @struct VulkanRenderer
 * @brief
 */
typedef struct vk_renderer_t {
    vector_t arrInstanceExtensions;
    vector_t arrDeviceExtensions;

    VkInstance hInstance;
    VkPhysicalDevice pGPU;
    VkDevice hDevice;
    vk_renderer_queue_t rendererQueue;

    VkSurfaceKHR hSurface;
    VkSwapchainKHR hSwapchain;

    // vector<VkImage>
    vector_t arrSwapchainImages;

    // vector<VkImageView>
    vector_t arrSwapchainImageViews;

    // vector<VkCommandBuffer>
    vector_t arrCmdBuffers;

    VkSemaphore hSemaphoreImageAvailable;
    VkSemaphore hSemaphoreRenderingFinished;

    SDL_Window* pSDLWindow;
} vk_renderer_t;

/**
 * @fn void MRenderer_Initialize()
 * @ingroup VulkanModule
 */
void MVulkan_InitializeRenderer(vk_renderer_t* renderer);

void MVulkan_UpdateRenderer(vk_renderer_t* renderer);

void MRenderer_Start();

void MRenderer_Update();

void MRenderer_Shutdown();

SX_RESULT MVulkan_LoadExtensionsForSDL(vector_t* extensions);

SX_RESULT MVulkan_CreateSDLSurface(SDL_Window* window, VkInstance instance, VkSurfaceKHR* surface);

#endif //SANDBOX_RENDERER_H
