//
// Copyright (c) E. A. Pristavka, 2017
//

#if HAVE_X11_XCB
#define VK_USE_PLATFORM_XCB_KHR

#include <X11/Xlib-xcb.h>

#endif

#include <SDL_syswm.h>
#include <vulkan/vulkan.h>
#include "renderer.h"

static const char* AppName = "Sandbox";
static const uint32_t WindowW = 1024;
static const uint32_t WindowH = 768;

void MVulkan_InitializeRenderer(vk_renderer_t* renderer) {

    renderer->pSDLWindow = SDL_CreateWindow(AppName, SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, WindowW, WindowH, 0);

    // Step 1. Create vulkan instance.

    // Initialize renderer inner structures.

    vector_init(&renderer->arrInstanceExtensions, NULL);
    vector_init(&renderer->arrDeviceExtensions, NULL);
    vector_append(&renderer->arrInstanceExtensions, VK_KHR_SURFACE_EXTENSION_NAME);

    // Define application info.

    VkApplicationInfo applicationInfo = {
            .sType = VK_STRUCTURE_TYPE_APPLICATION_INFO,
            .apiVersion = VK_API_VERSION_1_0,
            .applicationVersion = VK_MAKE_VERSION(0, 1, 0),
            .pApplicationName = AppName
    };

    // Load additional extensions for SDL if required.

    {
        SX_RESULT result = MVulkan_LoadExtensionsForSDL(&renderer->arrInstanceExtensions);

        if (result != SX_SUCCESS) {
            fprintf(stderr, "Vulkan Module: Error loading extensions for SDL!\n");
            assert(0);
        }
    }

    // Define instance create info.

    VkInstanceCreateInfo instanceCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
            .pApplicationInfo = &applicationInfo,
            .enabledExtensionCount = (uint32_t) renderer->arrInstanceExtensions.size,
            .ppEnabledExtensionNames = (const char* const*) renderer->arrInstanceExtensions.data
    };

    // Create instance.

    VkResult error = vkCreateInstance(&instanceCreateInfo, NULL, &renderer->hInstance);

    if (error != VK_SUCCESS) {
        fprintf(stderr, "Vulkan Module: Unable to create Vulkan instance!\n");
        assert(0);
    }

    fprintf(stdout, "Vulkan Module: Vulkan Instance created!\n");

    // Step 2. Create surface.

    {
        SX_RESULT result = MVulkan_CreateSDLSurface(renderer->pSDLWindow, renderer->hInstance, &renderer->hSurface);
        if (result != SX_SUCCESS) {
            fprintf(stderr, "Vulkan Module: Error creating SDL surface!\n");
            assert(0);
        }
    }

    // Step 3. Select GPU and queue.

    {
        renderer->pGPU = VK_NULL_HANDLE;

        uint32_t nGPUs = 0;
        error = vkEnumeratePhysicalDevices(renderer->hInstance, &nGPUs, NULL);

        assert(error == VK_SUCCESS && nGPUs > 0);

        if (nGPUs > 0) {
            VkPhysicalDevice arrGPUs[nGPUs];
            error = vkEnumeratePhysicalDevices(renderer->hInstance, &nGPUs, arrGPUs);

            assert(error == VK_SUCCESS);

            for (uint32_t iGPU = 0; iGPU < nGPUs; iGPU++) {

                uint32_t nQueueFamilies = 0;
                vkGetPhysicalDeviceQueueFamilyProperties(arrGPUs[iGPU], &nQueueFamilies, NULL);

                VkQueueFamilyProperties queueFamilies[nQueueFamilies];
                vkGetPhysicalDeviceQueueFamilyProperties(arrGPUs[iGPU], &nQueueFamilies, queueFamilies);

                uint32_t iSelectedQueueFamily = UINT32_MAX;

                for (uint32_t iQueueFamily = 0; iQueueFamily < nQueueFamilies; iQueueFamily++) {
                    VkBool32 bPresentationSupported = VK_FALSE;

                    vkGetPhysicalDeviceSurfaceSupportKHR(arrGPUs[iGPU], iQueueFamily, renderer->hSurface,
                                                         &bPresentationSupported);

                    if (bPresentationSupported && queueFamilies[iQueueFamily].queueFlags & VK_QUEUE_GRAPHICS_BIT) {
                        iSelectedQueueFamily = iQueueFamily;
                        break;
                    }
                }

                if (iSelectedQueueFamily != UINT32_MAX) {
                    renderer->pGPU = arrGPUs[iGPU];
                    renderer->rendererQueue.iQueueFamily = iSelectedQueueFamily;
                    break;
                }

            }
        }

        if (renderer->pGPU == VK_NULL_HANDLE) {
            fprintf(stderr, "Could not find compatible GPU.");
            assert(0);
        }
    }

    // Step 4. Create logical device.

    {
        float arrQueuePriorities[] = {1.0f};
        VkDeviceQueueCreateInfo deviceQueueCreateInfo = {
                .sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO,
                .queueFamilyIndex = renderer->rendererQueue.iQueueFamily,
                .queueCount = 1,
                .pQueuePriorities = arrQueuePriorities
        };

        VkDeviceCreateInfo deviceCreateInfo = {
                .sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO,
                .queueCreateInfoCount = 1,
                .pQueueCreateInfos = &deviceQueueCreateInfo,
                .enabledExtensionCount = (uint32_t) renderer->arrDeviceExtensions.size,
                .ppEnabledExtensionNames = (const char* const*) renderer->arrDeviceExtensions.data
        };

        error = vkCreateDevice(renderer->pGPU, &deviceCreateInfo, NULL, &renderer->hDevice);
        if (error != VK_SUCCESS) {
            fprintf(stderr, "Error calling vkCreateDevice");
            assert(0);
        }

        vkGetDeviceQueue(renderer->hDevice, renderer->rendererQueue.iQueueFamily, 0, &renderer->rendererQueue.hQueue);
    }

    // Step 5. Create swapchain.

    VkSurfaceFormatKHR surfaceFormat;
    {
        uint32_t nSurfaceFormats = 0;
        vkGetPhysicalDeviceSurfaceFormatsKHR(renderer->pGPU, renderer->hSurface, &nSurfaceFormats, NULL);

        VkSurfaceFormatKHR arrSurfaceFormats[nSurfaceFormats];
        vkGetPhysicalDeviceSurfaceFormatsKHR(renderer->pGPU, renderer->hSurface, &nSurfaceFormats, arrSurfaceFormats);

        if (nSurfaceFormats == 1 && arrSurfaceFormats[0].format == VK_FORMAT_UNDEFINED) {
            surfaceFormat.format = VK_FORMAT_B8G8R8A8_UNORM;
            surfaceFormat.colorSpace = VK_COLOR_SPACE_SRGB_NONLINEAR_KHR;
        } else {
            for (uint32_t i = 0; i < nSurfaceFormats; i++) {
                if (arrSurfaceFormats[i].format == VK_FORMAT_B8G8R8A8_UNORM &&
                    arrSurfaceFormats[i].colorSpace == VK_COLOR_SPACE_SRGB_NONLINEAR_KHR)
                    surfaceFormat = arrSurfaceFormats[i];
            }
        }

        if (!surfaceFormat.format)
            surfaceFormat = arrSurfaceFormats[0];
    }

    VkSurfaceCapabilitiesKHR surfaceCapabilities;
    vkGetPhysicalDeviceSurfaceCapabilitiesKHR(renderer->pGPU, renderer->hSurface, &surfaceCapabilities);

    VkPresentModeKHR presentMode = VK_PRESENT_MODE_FIFO_KHR;
    {
        uint32_t nPresentModes = 0;
        vkGetPhysicalDeviceSurfacePresentModesKHR(renderer->pGPU, renderer->hSurface, &nPresentModes, NULL);

        VkPresentModeKHR arrPresentModes[nPresentModes];
        vkGetPhysicalDeviceSurfacePresentModesKHR(renderer->pGPU, renderer->hSurface, &nPresentModes, arrPresentModes);

        for (int i = 0; i < nPresentModes; i++) {
            if (arrPresentModes[i] == VK_PRESENT_MODE_MAILBOX_KHR)
                presentMode = arrPresentModes[i];
        }
    }

    VkSwapchainCreateInfoKHR swapchainCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR,
            .surface = renderer->hSurface,
            .minImageCount = (surfaceCapabilities.maxImageCount < 3) ? surfaceCapabilities.maxImageCount : 3,
            .imageFormat = surfaceFormat.format,
            .imageColorSpace = surfaceFormat.colorSpace,
            .imageExtent = surfaceCapabilities.currentExtent,
            .imageArrayLayers = 1,
            .imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT,
            .imageSharingMode = VK_SHARING_MODE_EXCLUSIVE,
            .preTransform = surfaceCapabilities.currentTransform,
            .compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR,
            .presentMode = presentMode,
            .clipped = VK_TRUE,
            .oldSwapchain = VK_NULL_HANDLE,
    };

    error = vkCreateSwapchainKHR(renderer->hDevice, &swapchainCreateInfo, NULL, &renderer->hSwapchain);

    if (error != VK_SUCCESS) {
        fprintf(stderr, "Error calling vkCreateSwapchainKHR");
        assert(0);
    }

    // Step 6. Get images from swapchain.

    uint32_t nImages = 0;
    vkGetSwapchainImagesKHR(renderer->hDevice, renderer->hSwapchain, &nImages, NULL);
    vector_resize(&renderer->arrSwapchainImages, nImages);
    vkGetSwapchainImagesKHR(renderer->hDevice, renderer->hSwapchain, &nImages,
                            (VkImage*) renderer->arrSwapchainImages.data[0]);

    // Step 7. Fill command buffers.

    VkCommandPoolCreateInfo commandPoolCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
            .queueFamilyIndex = renderer->rendererQueue.iQueueFamily
    };

    error = vkCreateCommandPool(renderer->hDevice, &commandPoolCreateInfo, NULL, &renderer->rendererQueue.hCmdPool);

    if (error != VK_SUCCESS) {
        fprintf(stderr, "Error in vkCreateCommandPool");
        assert(0);
    }

    vector_resize(&renderer->arrCmdBuffers, nImages);

    VkCommandBufferAllocateInfo commandBufferAllocateInfo = {
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
            .commandPool = renderer->rendererQueue.hCmdPool,
            .level = VK_COMMAND_BUFFER_LEVEL_PRIMARY,
            .commandBufferCount = (uint32_t) renderer->arrCmdBuffers.size
    };

    error = vkAllocateCommandBuffers(renderer->hDevice, &commandBufferAllocateInfo,
                                     (VkCommandBuffer*) renderer->arrCmdBuffers.data[0]);

    if (error != VK_SUCCESS) {
        fprintf(stderr, "Error in vkAllocateCommandBuffers");
        assert(0);
    }

    // Step 8. Set image barriers.

    VkCommandBufferBeginInfo commandBufferBeginInfo = {
            .sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
            .flags = VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT
    };

    VkClearColorValue clearColorValue = {1.0f, 0.5f, 0};

    VkImageSubresourceRange imageSubresourceRange = {
            .aspectMask = VK_IMAGE_ASPECT_COLOR_BIT,
            .baseMipLevel = 0,
            .levelCount = 1,
            .baseArrayLayer = 0,
            .layerCount = 1
    };

    for (uint32_t i = 0; i < renderer->arrSwapchainImages.size; i++) {
        VkImageMemoryBarrier imageMemoryBarrierPresentToClear = {
                .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
                .srcAccessMask = VK_ACCESS_MEMORY_READ_BIT,
                .dstAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
                .oldLayout = VK_IMAGE_LAYOUT_UNDEFINED,
                .newLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                .srcQueueFamilyIndex = renderer->rendererQueue.iQueueFamily,
                .dstQueueFamilyIndex = renderer->rendererQueue.iQueueFamily,
                .image = ((VkImage*) renderer->arrSwapchainImages.data[0])[i],
                .subresourceRange = imageSubresourceRange
        };

        VkImageMemoryBarrier imageMemoryBarrierClearToPresent = {
                .sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
                .srcAccessMask = VK_ACCESS_TRANSFER_WRITE_BIT,
                .dstAccessMask = VK_ACCESS_MEMORY_READ_BIT,
                .oldLayout = VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                .newLayout = VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
                .srcQueueFamilyIndex = renderer->rendererQueue.iQueueFamily,
                .dstQueueFamilyIndex = renderer->rendererQueue.iQueueFamily,
                .image = ((VkImage*) renderer->arrSwapchainImages.data[0])[i],
                .subresourceRange = imageSubresourceRange
        };

        vkBeginCommandBuffer(renderer->arrCmdBuffers.data[i], &commandBufferBeginInfo);
        vkCmdPipelineBarrier(renderer->arrCmdBuffers.data[i],
                             VK_PIPELINE_STAGE_TRANSFER_BIT,
                             VK_PIPELINE_STAGE_TRANSFER_BIT,
                             (VkDependencyFlags) 0,
                             (uint32_t) 0, (const VkMemoryBarrier*) NULL,
                             (uint32_t) 0, (const VkBufferMemoryBarrier*) NULL,
                             (uint32_t) 1, &imageMemoryBarrierPresentToClear
        );

        vkCmdClearColorImage(renderer->arrCmdBuffers.data[i],
                             renderer->arrSwapchainImages.data[i],
                             VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                             &clearColorValue,
                             (uint32_t) 1, &imageSubresourceRange
        );

        vkCmdPipelineBarrier(renderer->arrCmdBuffers.data[i],
                             VK_PIPELINE_STAGE_TRANSFER_BIT,
                             VK_PIPELINE_STAGE_TRANSFER_BIT,
                             (VkDependencyFlags) 0,
                             (uint32_t) 0, (const VkMemoryBarrier*) NULL,
                             (uint32_t) 0, (const VkBufferMemoryBarrier*) NULL,
                             (uint32_t) 1, &imageMemoryBarrierPresentToClear
        );

        vkEndCommandBuffer(renderer->arrCmdBuffers.data[i]);
    }

    // Step 9. Set semaphore.

    VkSemaphoreCreateInfo semaphoreCreateInfo = {
            .sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO
    };

    error = vkCreateSemaphore(renderer->hDevice,
                              &semaphoreCreateInfo,
                              (const VkAllocationCallbacks*) NULL,
                              &renderer->hSemaphoreImageAvailable);

    if (error != VK_SUCCESS) {
        fprintf(stderr, "Error in vkCreateSemaphore");
        assert(0);
    }
}

SX_RESULT MVulkan_LoadExtensionsForSDL(vector_t* extensions) {
    const char* driver = SDL_GetCurrentVideoDriver();

    if (!driver) {
        fprintf(stderr, "No video driver - has SDL_Init(SDL_INIT_VIDEO) been called?\n");
        return SX_FAILURE;
    }

    if (!extensions->size) {
        fprintf(stderr, "No extensions to process!\n");
        return SX_FAILURE;
    }

    #if HAVE_X11_XCB
    if (!strcmp(driver, "x11")) {
        vector_append(extensions, VK_KHR_XCB_SURFACE_EXTENSION_NAME);
    }
    #endif

    return SX_SUCCESS;
}

SX_RESULT MVulkan_CreateSDLSurface(SDL_Window* window, VkInstance instance, VkSurfaceKHR* surface) {

    if (!window) {
        fprintf(stderr, "Window parameter is NULL\n");
        return SX_FAILURE;
    }

    if (instance == VK_NULL_HANDLE) {
        fprintf(stderr, "Instance parameter is NULL\n");
        return SX_FAILURE;
    }

    SDL_SysWMinfo wmInfo;

    SDL_VERSION(&wmInfo.version);

    if (!SDL_GetWindowWMInfo(window, &wmInfo)) {
        return SX_FAILURE;
    }

    switch (wmInfo.subsystem) {
#if HAVE_X11_XCB
        case SDL_SYSWM_X11: {
            const VkXcbSurfaceCreateInfoKHR createInfo = {
                    .sType = VK_STRUCTURE_TYPE_XCB_SURFACE_CREATE_INFO_KHR,
                    .pNext = NULL,
                    .flags = 0,
                    .connection = XGetXCBConnection(wmInfo.info.x11.display),
                    .window = (xcb_window_t) wmInfo.info.x11.window,
            };

            VkResult result = vkCreateXcbSurfaceKHR(instance, &createInfo, NULL, surface);

            if (result != VK_SUCCESS) {
                fprintf(stderr, "vkCreateXcbSurfaceKHR failed: %i\n", result);
                return SX_FAILURE;
            }

            return SX_SUCCESS;
        }
#endif
        default:
            SDL_SetError("Unsupported subsystem %i\n", wmInfo.subsystem);
            return SX_FAILURE;
    }

    return SX_SUCCESS;
}

void MVulkan_UpdateRenderer(vk_renderer_t* renderer) {
    uint32_t acquireNextImage;

    VkResult error = vkAcquireNextImageKHR(renderer->hDevice, renderer->hSwapchain, UINT64_MAX,
                                           renderer->hSemaphoreImageAvailable, VK_NULL_HANDLE, &acquireNextImage);

    if (error != VK_SUCCESS) {
        fprintf(stderr, "Error in vkAcquireNextImageKHR");
        assert(0);
    }

    VkPipelineStageFlags pipelineStageFlags = VK_PIPELINE_STAGE_TRANSFER_BIT;

    VkSubmitInfo submitInfo = {
            .sType = VK_STRUCTURE_TYPE_SUBMIT_INFO,
            .waitSemaphoreCount = 1,
            .pWaitSemaphores = &renderer->hSemaphoreImageAvailable,
            .pWaitDstStageMask = &pipelineStageFlags,
            .commandBufferCount = 1,
            .pCommandBuffers = renderer->arrCmdBuffers.data[acquireNextImage],
            .signalSemaphoreCount = 1,
            .pSignalSemaphores = &renderer->hSemaphoreRenderingFinished
    };

    error = vkQueueSubmit(renderer->rendererQueue.hQueue, 1, &submitInfo, VK_NULL_HANDLE);

    if (error != VK_SUCCESS) {
        fprintf(stderr, "Error in vkQueueSubmit");
        assert(0);
    }

    VkPresentInfoKHR presentInfoKHR = {
            .sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,
            .waitSemaphoreCount = 1,
            .pWaitSemaphores = &renderer->hSemaphoreRenderingFinished,
            .swapchainCount = 1,
            .pSwapchains = &renderer->hSwapchain,
            .pImageIndices = &acquireNextImage
    };

    error = vkQueuePresentKHR(renderer->rendererQueue.hQueue, &presentInfoKHR);

    if (error != VK_SUCCESS) {
        fprintf(stderr, "Error in vkQueuePresentKHR");
        assert(0);
    }
}
