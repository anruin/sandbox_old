cmake_minimum_required(VERSION 3.5)

set(SOURCE_FILES
        ${PROJECT_SOURCE_DIR}/src/modules/vulkan/vulkan_module.c
        ${PROJECT_SOURCE_DIR}/src/modules/vulkan/SDL_vulkan/SDL_vulkan.c
        ${PROJECT_SOURCE_DIR}/src/modules/vulkan/renderer.c
        )

find_package(SDL2 REQUIRED)
if (NOT SDL2_INCLUDE_DIR OR NOT SDL2_LIBRARY)
    message(FATAL_ERROR "-- SDL2 was not found")
endif ()

find_package(Vulkan REQUIRED)
if (NOT VULKAN_FOUND)
    message(FATAL_ERROR "-- Vulkan was not found")
endif ()

set(API_INCLUDE_DIR ${PROJECT_SOURCE_DIR}/src/api/ ${PROJECT_SOURCE_DIR}/src/application/)

if (UNIX)
    add_definitions(-DHAVE_X11_XCB)
    set(ADDITIONAL_LIBS libX11-xcb.a)
endif()

include_directories(${VULKAN_INCLUDE_DIR} ${SDL2_INCLUDE_DIR} ${API_INCLUDE_DIR} )

add_library(VulkanModule MODULE ${SOURCE_FILES})
target_link_libraries(VulkanModule PUBLIC ${ADDITIONAL_LIBS} ${SDL2_LIBRARY} ${VULKAN_LIBRARY})
