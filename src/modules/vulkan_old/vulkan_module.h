/**
 * @file vulkan_module.h
 * @defgroup VulkanModule Vulkan Graphics Module
 * @ingroup ModuleAPI
 * @brief Sandbox Vulkan Graphics Module.
 * @details Vulkan Graphics Module.
 * @author Egor Pristavka
 * @copyright Egor Pristavka 2017
 */

#ifndef SANDBOX_EXAMPLE_MODULE_H
#define SANDBOX_EXAMPLE_MODULE_H

#include "module.h"

DECLARE_MODULE_INTERFACE()

#endif //SANDBOX_EXAMPLE_MODULE_H
