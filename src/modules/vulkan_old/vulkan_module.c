//
// Copyright (c) E. A. Pristavka, 2017
//

#include <stdio.h>
#include <SDL_events.h>
#include <vulkan/vulkan.h>
#include <assert.h>
#include <SDL.h>
#include "vulkan_module.h"
#include "SDL_vulkan/SDL_vulkan.h"
#include "renderer.h"

static const char* ApplicationName = "Sandbox Vulkan";
static const int ScreenWidth = 1024;
static const int ScreenHeight = 768;

static vk_renderer_t renderer;

void MODULE_INITIALIZE() {
    if (SDL_Init(SDL_INIT_VIDEO) != 0)
        printf("CoreModule: Error initializing SDL: %s\n", SDL_GetError());

    MVulkan_InitializeRenderer(&renderer);
}

void MODULE_START() {

}

void MODULE_UPDATE() {

}

void MODULE_SHUTDOWN() {
    printf("Vulkan module unloaded!\n");
}

function_list_t* MODULE_GET_FUNCTION_LIST() {
    return NULL;
}

void* MODULE_CALL_FUNCTION(const char* name, void** parameters) {
    return NULL;
}

void MODULE_ON_EVENT(event_t* event) {
}