//
// Copyright (c) E. A. Pristavka, 2017
//

#ifndef SANDBOX_CORE_MODULE_H
#define SANDBOX_CORE_MODULE_H

#include <SDL.h>
#include "api/module.h"

DECLARE_MODULE_INTERFACE()

#endif //SANDBOX_CORE_MODULE_H
