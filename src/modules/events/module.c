//
// Copyright (c) E. A. Pristavka, 2017
//

#include "api/application.h"
#include "module.h"

static void MCore_OnSDLEvent(SDL_Event *event);

void MODULE_ON_CREATE() {

    if (SDL_InitSubSystem(SDL_INIT_EVENTS) != 0) {
        DEBUG_PRINT("MEvents got an error initializing SDL: %s\n", SDL_GetError());
    }

    DEBUG_PRINT("MEvents created.\n");

}

void MODULE_ON_READY() {

}

void MODULE_ON_UPDATE() {

    SDL_Event Event;

    // Process SDL events

    while (SDL_PollEvent(&Event) != 0) {
        MCore_OnSDLEvent(&Event);
    }
}

void MODULE_ON_DESTROY() {

    SDL_QuitSubSystem(SDL_INIT_EVENTS);

    SDL_Quit();

    DEBUG_PRINT("MEvents unloaded.\n");

}

void *MODULE_CALL(const char *name, void **parameters) {
    (void)name;
    (void)parameters;

    return NULL;
}

void MODULE_ON_EVENT(CEvent *pEvent) {
    DEBUG_PRINT("MEvents received event %s.\n", pEvent->arrTopic);
}

static void MCore_OnSDLEvent(SDL_Event *event) {

    if (event->type == SDL_QUIT) {

        CEvent *pEvent = CEvent_New("CApplication_Shutdown", 1, NULL, 0);
        CApplication_PushEvent(pEvent);

    }

}